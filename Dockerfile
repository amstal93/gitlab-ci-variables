FROM registry.gitlab.com/youtopia.earth/ansible

MAINTAINER Jo <idetoile@protonmail.com>

COPY app/bin/* /bin/
COPY app /app

WORKDIR /app

ENV EDITOR nano
